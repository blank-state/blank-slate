FROM georgejose/node:latest

RUN npm install -g pushstate-server
COPY package.json /tmp/
RUN cd /tmp && yarn install

COPY . /app
RUN rm -rf node_modules
RUN mv /tmp/node_modules /app/node_modules

WORKDIR /app/
CMD yarn build && pushstate-server build/ 9000
